// import global stylesheet
import "./style.css"

import { slides as cover } from "./slides/1-cover.mdx"
import { slides as aboutme } from "./slides/2-aboutme.mdx"
import { slides as aboutdeck } from "./slides/3-aboutdeck.mdx"
import { slides as content } from "./slides/4-content.mdx"
import { slides as closing } from "./slides/5-closing.mdx"

// import `slides` from your mdx files and spread into `slides` export
// theme export for code-surfer
export { github as theme } from "code-surfer"

// theme exports for mdx-deck since `theme` is used by code-surfer
export const themes = []

// Spread your slides here...
export const slides = [
  ...cover,
  ...aboutme,
  ...aboutdeck,
  ...content,
  ...closing,
]
